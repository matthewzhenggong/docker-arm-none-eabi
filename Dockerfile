FROM debian:stretch-slim
MAINTAINER https://gitlab.com/alelec/docker-arm-none-eabi

RUN sed -ri 's/^# deb /deb /g' /etc/apt/sources.list && \
    apt update; \
    apt install -y gnupg build-essential git-core ca-certificates libltdl-dev \
    python python-dev python-pip python3 python3-dev python3-pip ninja-build \
    gcc-multilib pkg-config libffi-dev qemu-system gcc-mingw-w64 cmake zip \
    autoconf autotools-dev automake autogen libtool m4 realpath gettext

RUN pip install -U pip setuptools wheel && \
    pip3 install -U pip setuptools wheel pipenv cpp-coveralls

# Install gcc
ENV VERSION="8-2019q3"
RUN URL="https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2019q3/RC1.1/gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2" && \
    cd /opt &&\
    curl -L "$URL" > gcc.tbz &&\
    tar xvf gcc.tbz &&\
    rm gcc.tbz &&\
    ls

ENV PATH=/opt/gcc-arm-none-eabi-8-2019-q3-update/bin:$PATH

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
    
# Print out installed version
RUN echo "VERSION=$VERSION"

# Clean up cache
RUN rm -rf /var/cache/apk/*
